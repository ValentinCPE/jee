package com.cpe.jee.controller;

import com.cpe.jee.exception.GeneralHandlerException;
import com.cpe.jee.model.dto.UserDto;
import com.cpe.jee.model.view.ResponseView;
import com.cpe.jee.model.view.UserView;
import com.cpe.jee.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/user")
public class UserController {

    @Autowired
    private UserService userService;


    @PostMapping(value = "/login", produces = { "application/json" }) /* @Valid à côté de @RequestBody permet de dire à Spring de controller les contraintes rentrées dans UserDto lors de la réception de la requête, dans ce cas précis @NotNull. Si c'est null, Spring renverra une erreur lors de la requête */
    public ResponseEntity<ResponseView> login(@RequestBody @Valid UserDto userDto){

        ResponseEntity<ResponseView> response;
        ResponseView responseViewReturn = new ResponseView();

        try {

            UserView userView = userService.login(userDto);

            //Retour normal sans exception de notre méthode dans le service
            responseViewReturn.setStatusHttp(200);
            responseViewReturn.setData(userView);

        } catch (GeneralHandlerException e) {

            //Garder le même objet ResponseView permet d'avoir une structure de JSON identique en réponse de la requête qu'on est levé une exception ou pas. Ici une exception a été levé.
            responseViewReturn.setStatusHttp(e.getStatusHttp().value());
            responseViewReturn.setData(e.getMessage());

        } finally {

            //Configuration ResponseEntity correct pour être renvoyé par le controlleur
            response = ResponseEntity.status(responseViewReturn.getStatusHttp()).body(responseViewReturn);

        }

        return response;

    }

    @PostMapping(value = "/create", produces = { "application/json" })
    public ResponseEntity<ResponseView> create(@RequestBody @Valid UserDto userDto){

        ResponseEntity<ResponseView> response;
        ResponseView responseViewReturn = new ResponseView();

        try {
            UserView userView = userService.create(userDto);

            responseViewReturn.setStatusHttp(200);
            responseViewReturn.setData(userView);

        } catch (GeneralHandlerException e) {
            responseViewReturn.setStatusHttp(e.getStatusHttp().value());
            responseViewReturn.setData(e.getMessage());
        } finally {
            response = ResponseEntity.status(responseViewReturn.getStatusHttp()).body(responseViewReturn);
        }

        return response;

    }

    // Route seulement accessible si token valide passé dans le header de la requête HTTP : Authorization. ROUTE SECURISEE
    @GetMapping(value = "/getFromToken", produces = { "application/json" })
    public ResponseEntity<ResponseView> getFromToken(/* Permet de récupérer le header authorization */@RequestHeader(value="Authorization") String token){

        ResponseEntity<ResponseView> response;
        ResponseView responseViewReturn = new ResponseView();

        try {
            UserView userView = userService.getUserWithToken(token);

            responseViewReturn.setStatusHttp(200);
            responseViewReturn.setData(userView);

        } catch (GeneralHandlerException e) {
            responseViewReturn.setStatusHttp(e.getStatusHttp().value());
            responseViewReturn.setData(e.getMessage());
        } finally {
            response = ResponseEntity.status(responseViewReturn.getStatusHttp()).body(responseViewReturn);
        }

        return response;

    }

}
