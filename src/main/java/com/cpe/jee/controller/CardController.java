package com.cpe.jee.controller;

import com.cpe.jee.exception.GeneralHandlerException;
import com.cpe.jee.model.dto.AchatCardDto;
import com.cpe.jee.model.dto.CardDto;
import com.cpe.jee.model.dto.UserDto;
import com.cpe.jee.model.dto.VenteCardDto;
import com.cpe.jee.model.view.CardView;
import com.cpe.jee.model.view.ResponseView;
import com.cpe.jee.model.view.UserView;
import com.cpe.jee.service.CardService;
import com.cpe.jee.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/card")
public class CardController {

    @Autowired
    private CardService cardService;


    // Route seulement accessible si token valide passé dans le header de la requête HTTP : Authorization. ROUTE SECURISEE
    @GetMapping(value = "/list", produces = { "application/json" })
    public ResponseEntity<ResponseView> list(/* Permet de récupérer le header authorization */@RequestHeader(value="Authorization") String token){
        ResponseEntity<ResponseView> response;
        ResponseView responseViewReturn = new ResponseView();

        try {
            Iterable<CardView> cardsView = cardService.listCards(token);
            responseViewReturn.setStatusHttp(200);
            responseViewReturn.setData(cardsView);

        } catch (GeneralHandlerException e) {
            responseViewReturn.setStatusHttp(e.getStatusHttp().value());
            responseViewReturn.setData(e.getMessage());
        } finally {
            response = ResponseEntity.status(responseViewReturn.getStatusHttp()).body(responseViewReturn);
        }

        return response;

    }

    @PostMapping(value = "/create", produces = { "application/json" })
    public ResponseEntity<ResponseView> create(@RequestBody @Valid CardDto card ,@RequestHeader(value="Authorization") String token){
        ResponseEntity<ResponseView> response;
        ResponseView responseViewReturn = new ResponseView();

        try {
            CardView cardView = cardService.create(token,card);
            responseViewReturn.setStatusHttp(200);
            responseViewReturn.setData(cardView);

        } catch (GeneralHandlerException e) {
            responseViewReturn.setStatusHttp(e.getStatusHttp().value());
            responseViewReturn.setData(e.getMessage());
        } finally {
            response = ResponseEntity.status(responseViewReturn.getStatusHttp()).body(responseViewReturn);
        }

        return response;

    }

    @GetMapping(value = "/get/{name}", produces = { "application/json" })
    public ResponseEntity<ResponseView> create(@PathVariable(value="name") String name,@RequestHeader(value="Authorization") String token){
        ResponseEntity<ResponseView> response;
        ResponseView responseViewReturn = new ResponseView();

        try {
            CardView cardView = cardService.getCardByName(token,name);
            responseViewReturn.setStatusHttp(200);
            responseViewReturn.setData(cardView);

        } catch (GeneralHandlerException e) {
            responseViewReturn.setStatusHttp(e.getStatusHttp().value());
            responseViewReturn.setData(e.getMessage());
        } finally {
            response = ResponseEntity.status(responseViewReturn.getStatusHttp()).body(responseViewReturn);
        }

        return response;

    }

    @PutMapping(value = "/vendre", produces = { "application/json" })
    public ResponseEntity<ResponseView> vendre(@RequestHeader(value="Authorization") String token, @RequestBody @Valid VenteCardDto venteCardDto){
        ResponseEntity<ResponseView> response;
        ResponseView responseViewReturn = new ResponseView();

        try {
            List<CardView> cardsEnVente = cardService.vendreCards(token, venteCardDto);
            responseViewReturn.setStatusHttp(200);
            responseViewReturn.setData(cardsEnVente);

        } catch (GeneralHandlerException e) {
            responseViewReturn.setStatusHttp(e.getStatusHttp().value());
            responseViewReturn.setData(e.getMessage());
        } finally {
            response = ResponseEntity.status(responseViewReturn.getStatusHttp()).body(responseViewReturn);
        }

        return response;

    }

    @PutMapping(value = "/acheter", produces = { "application/json" })
    public ResponseEntity<ResponseView> acheter(@RequestHeader(value="Authorization") String token, @RequestBody @Valid AchatCardDto achatCardDto){
        ResponseEntity<ResponseView> response;
        ResponseView responseViewReturn = new ResponseView();

        try {
            UserView userAcheteur = cardService.achatCards(token, achatCardDto);
            responseViewReturn.setStatusHttp(200);
            responseViewReturn.setData(userAcheteur);

        } catch (GeneralHandlerException e) {
            responseViewReturn.setStatusHttp(e.getStatusHttp().value());
            responseViewReturn.setData(e.getMessage());
        } finally {
            response = ResponseEntity.status(responseViewReturn.getStatusHttp()).body(responseViewReturn);
        }

        return response;

    }

}
