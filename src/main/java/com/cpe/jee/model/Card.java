package com.cpe.jee.model;

import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "Card")
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Card {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @NotNull
    @Column(unique = true)
    private String name;

    private String description;

    @NotNull
    private String family;

    @NotNull
    private Integer hp;

    @NotNull
    private Integer energy;

    @NotNull
    private Integer defence;

    @NotNull
    private Integer attack;

    @NotNull
    private String imgUrl;

    private boolean enVente = false;

    private long price;

}
