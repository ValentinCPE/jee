package com.cpe.jee.model.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CardDto {


    private String name;

    private String description;

    private String family;

    private Integer hp;

    private Integer energy;

    private Integer defence;

    private Integer attack;

    private String imgUrl;

}
