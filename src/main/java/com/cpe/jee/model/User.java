package com.cpe.jee.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "User", indexes = { @Index(name = "IDX_Mail_User", columnList = "mail") })
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    private String name;

    @NotNull //Champ ne peut pas être null
    @Column(unique = true) //Impossible d'avoir 2 adresses mail identiques
    private String mail;

    private String password;

    private Timestamp date_creation;

    @OneToOne
    private Token token;

    @OneToMany(fetch = FetchType.EAGER)
    private List<Card> cards = new ArrayList<>();

    private Double solde = 0.0;

}
