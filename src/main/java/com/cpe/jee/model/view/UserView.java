package com.cpe.jee.model.view;

import com.cpe.jee.model.User;
import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class UserView {

    private String name;

    private String mail;

    private String token_key;

    private List<CardView> cards;

    private Double solde;

    public static UserView getUserViewFromUser(User user){
        UserView userView = UserView.builder()
                .name(user.getName())
                .mail(user.getMail())
                .cards(CardView.getListCardViewFromCards(user.getCards()))
                .solde(user.getSolde())
                .build();

        if(user.getToken() != null){
            userView.setToken_key(user.getToken().getKeyToken());
        }

        return userView;
    }

}
