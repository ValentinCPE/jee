package com.cpe.jee.model.view;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class ResponseView {

    private int statusHttp;

    private Object data;

}
