package com.cpe.jee.model.view;

import com.cpe.jee.model.Card;
import lombok.Builder;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@Data
@Builder
public class CardView {

    private String name;

    private String description;

    private String family;

    private Integer hp;

    private Integer energy;

    private Integer defence;

    private Integer attack;

    private String imgUrl;

    private boolean enVente;

    private long price;

    public static CardView getCardViewFromCard(Card card){
        return CardView.builder()
                .name(card.getName())
                .description(card.getDescription())
                .family(card.getFamily())
                .hp(card.getHp())
                .energy(card.getEnergy())
                .defence(card.getDefence())
                .attack(card.getAttack())
                .imgUrl(card.getImgUrl())
                .enVente(card.isEnVente())
                .price(card.getPrice())
                .build();
    }

    public static List<CardView> getListCardViewFromCards(List<Card> cards){
        List<CardView> cardViews = new ArrayList<>();

        if(cards != null){
            for(Card card : cards){
                cardViews.add(CardView.getCardViewFromCard(card));
            }
        }

        return cardViews;
    }

    public static Iterable<CardView> getIterableCardViewFromIterableCard(Iterable<Card> cards){
        List<CardView> cardsView = new ArrayList<CardView>();
        for (Card card:cards) {
            cardsView.add(CardView.getCardViewFromCard(card));
        }
        return cardsView;
    }

}
