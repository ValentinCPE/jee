package com.cpe.jee.repository;

import com.cpe.jee.model.Card;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CardRepository extends CrudRepository<Card, Integer>{
    Iterable<Card> findAll();
    Card findCardByName(String name);
}
