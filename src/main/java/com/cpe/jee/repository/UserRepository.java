package com.cpe.jee.repository;

import com.cpe.jee.model.Card;
import com.cpe.jee.model.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends CrudRepository<User, Integer>{

    User findUserByMail(String mail);

    //Permet de dire : tu me récupères le user qui a comme attribut keyToken de son Token le paramètre
    User findUserByToken_KeyToken(String token);

    User findUserByCardsContains(Card card);

}
