package com.cpe.jee.repository;

import com.cpe.jee.model.Token;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TokenRepository extends CrudRepository<Token, Integer>{

    Token findTokenById(Integer id);

}
