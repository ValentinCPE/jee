package com.cpe.jee.service;

import com.cpe.jee.exception.*;
import com.cpe.jee.model.Card;
import com.cpe.jee.model.dto.AchatCardDto;
import com.cpe.jee.model.dto.CardDto;
import com.cpe.jee.model.dto.VenteCardDto;
import com.cpe.jee.model.view.CardView;
import com.cpe.jee.model.view.UserView;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;

@Service
public interface CardService {

    Iterable<CardView> listCards(String token) throws NoCardException, NotConnectedException;

    CardView create(String token, CardDto card) throws NotConnectedException, CardAlreadyExistException, ParameterException;

    CardView getCardByName(String token, String name) throws NotConnectedException, ParameterException, CardDoesntExistException;

    List<CardView> vendreCards(String token, VenteCardDto venteCardDto) throws NotConnectedException;

    UserView achatCards(String token, AchatCardDto achatCardDto) throws NotConnectedException;

    Set<Card> getFiveRandomCard();

}
