package com.cpe.jee.service.impl;

import com.cpe.jee.exception.*;
import com.cpe.jee.model.Card;
import com.cpe.jee.model.Token;
import com.cpe.jee.model.User;
import com.cpe.jee.model.dto.UserDto;
import com.cpe.jee.model.view.UserView;
import com.cpe.jee.repository.UserRepository;
import com.cpe.jee.service.CardService;
import com.cpe.jee.service.TokenService;
import com.cpe.jee.service.UserService;
import org.mindrot.jbcrypt.BCrypt;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.Set;
import java.util.UUID;
import java.util.regex.Pattern;

@Service
public class UserServiceImpl implements UserService {

    //Regex email address
    public static final Pattern VALID_EMAIL_ADDRESS_REGEX =
            Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);

    @Autowired
    private UserRepository userRepository;

    //Référence à un service dédié token et non pas un repo autre que User
    @Autowired
    private TokenService tokenService;

    @Autowired
    private CardService cardService;

    @Override
    public UserView getUserFromName(String name) throws ParameterException {

        if(name.isEmpty()){
           throw new ParameterException(HttpStatus.BAD_REQUEST, "name");
        }

       // User userFound = userRepository

        return null;

    }

    @Override
    public UserView login(UserDto userFormulaire) throws ParameterException, MailNotFoundException, PasswordIncorrectException {

        //Non nécessaire car Spring aurait levé une erreur lors de la requête mais on ne sait jamais
        if(userFormulaire == null){
            throw new ParameterException(HttpStatus.BAD_REQUEST, "Form JSON");
        }

        if(userFormulaire.getMail().isEmpty()){
            throw new ParameterException(HttpStatus.BAD_REQUEST, "Mail attribute");
        }

        //Récupération User BDD si adresse mail a un compte
        User userDB = userRepository.findUserByMail(userFormulaire.getMail());

        //Si user null, alors compte n'existe pas
        if(userDB == null){
            throw new MailNotFoundException(HttpStatus.UNAUTHORIZED, userFormulaire.getMail());
        }

        //Ici, nous savons que le User avec cette adresse mail existe, comparons désormais les mots de passe. Une fonction nous permet de savoir si le hashage du mot de passe entré dans le formulaire correspond bien au hash stocké en BDD, sinon exception levée.
        if(!BCrypt.checkpw(userFormulaire.getPassword(), userDB.getPassword())){
            //Grace au ! au début, nous indiquons que si ça ne matche pas, exception levée
            throw new PasswordIncorrectException(HttpStatus.UNAUTHORIZED);
        }

        //User doit désormais être connecté. Informations vérifiées. Création donc d'un token ou modification d'un ancien token.
        Token newToken = Token.builder()
                .keyToken(UUID.randomUUID().toString()) //Génération string aléatoire
                .creation_date(new Timestamp(new Date().getTime())) //timestamp de maintenant
                .build();
        tokenService.saveNewToken(newToken); //Obligé de l'ajouter en BDD Token avant de le lier à un User sinon erreur

        //Ancien token du user à changer puis à supprimer
        Token oldToken = userDB.getToken();

        //Affectation d'un nouveau token
        userDB.setToken(newToken);

        //Enregistrement user modifié en BDD
        userRepository.save(userDB);

        //Suppression ancien token de BDD token
        if(oldToken != null){
            tokenService.removeToken(oldToken.getId()); //Suppression ancien token si user en possédait un
        }

        //Création et return de JSON UserView qui sera envoyé en réponse de la requête HTTP
        return UserView.getUserViewFromUser(userDB);

    }

    @Override
    public UserView create(UserDto userFormulaire) throws ParameterException, AccountAlreadyExistsException, AcceptationCriteriaRefusedException {

        if(userFormulaire == null){
            throw new ParameterException(HttpStatus.BAD_REQUEST, "Form JSON");
        }

        if(userFormulaire.getMail().isEmpty()){
            throw new ParameterException(HttpStatus.BAD_REQUEST, "Mail attribute");
        }

        // Nous vérifions si un compte n'existe pas déjà avec cette adresse mail
        if(userRepository.findUserByMail(userFormulaire.getMail()) != null){
            throw new AccountAlreadyExistsException(HttpStatus.BAD_REQUEST, userFormulaire.getMail());
        }

        //On appelle une fonction statique de la classe qui va se charder d'inspecter les rêgles élémentaires des différents champs (email, password...). Ne jamais faire confiance à un client, revérifier côté serveur.
        if(!UserServiceImpl.areValuesCorrectWithCriteria(userFormulaire.getMail(), userFormulaire.getPassword())){
            throw new AcceptationCriteriaRefusedException(HttpStatus.BAD_REQUEST);
        }
        Set<Card> cards = cardService.getFiveRandomCard();
        //Désormais, on est sur qu'utilisateur avec cette adresse n'existe pas déjà, les rêgles pour la création sont respectées. Nous pouvons créer cet utilisateur.
        User newUser = User.builder()
                .name(userFormulaire.getName())
                .mail(userFormulaire.getMail())
                .password(BCrypt.hashpw(userFormulaire.getPassword(), BCrypt.gensalt())) //Nous hashons le mot de passe pour la BDD
                .token(null) //Un token sera généré lors du login seulement et non pas à l'inscription. Obligation de se connecter apres une inscription
                .date_creation(new Timestamp(new Date().getTime()))
                .cards(new ArrayList<>())
                .solde(0.0)
                .build();

        userRepository.save(newUser); //User sauvegardé en BDD

        return UserView.getUserViewFromUser(newUser);

    }

    @Override
    public UserView getUserWithToken(String token) throws NotConnectedException {

        if(token == null || token.isEmpty()){
            throw new NotConnectedException(HttpStatus.UNAUTHORIZED);
        }

        User userFound = userRepository.findUserByToken_KeyToken(token);

        if(userFound == null){
            throw new NotConnectedException(HttpStatus.UNAUTHORIZED);
        }

        return UserView.getUserViewFromUser(userFound);

    }

    @Override
    public UserView logout(String token) throws NotConnectedException {

        if(token == null || token.isEmpty()){
            throw new NotConnectedException(HttpStatus.UNAUTHORIZED);
        }

        User userFound = userRepository.findUserByToken_KeyToken(token);

        if(userFound == null){
            throw new NotConnectedException(HttpStatus.UNAUTHORIZED);
        }

        Token currentToken = userFound.getToken();

        userFound.setToken(null);

        userRepository.save(userFound);

        if(currentToken != null){
            tokenService.removeToken(currentToken.getId());
        }

        return UserView.getUserViewFromUser(userFound);

    }

    @Override
    public boolean isConnected(String token) {
        if(token == null || token.isEmpty()){
            return false;
        }
        User userFound = userRepository.findUserByToken_KeyToken(token);
        return userFound != null;
    }

    @Override
    public User getUser(String token) {
        if(token == null || token.isEmpty()){
            return null;
        }
        return userRepository.findUserByToken_KeyToken(token);
    }

    @Override
    public User getUserWithCard(Card card) {
        return userRepository.findUserByCardsContains(card);
    }

    @Override
    public void saveUser(User user) {
        if(user != null){
            userRepository.save(user);
        }
    }

    @Override
    public User getUserFromMail(String mail) {
        return userRepository.findUserByMail(mail);
    }

    private static boolean areValuesCorrectWithCriteria(String mail, String password){
        //Pour l'instant, nous vérifions seulement que le format de l'email semble correct avec une regex et que le mot de passe fait plus de 6 caractères. On pourra l'étoffer à tout moment avec l'ajout de && à l'expression (controle de regex, tailles,....)
        return VALID_EMAIL_ADDRESS_REGEX.matcher(mail).find() && password.length() > 6;
    }

}
