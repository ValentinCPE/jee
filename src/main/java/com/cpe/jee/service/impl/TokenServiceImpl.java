package com.cpe.jee.service.impl;

import com.cpe.jee.model.Token;
import com.cpe.jee.repository.TokenRepository;
import com.cpe.jee.service.TokenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TokenServiceImpl implements TokenService {

    @Autowired
    private TokenRepository tokenRepository;


    @Override
    public void removeToken(Integer idToken) {
        if(idToken != null){
            tokenRepository.delete(idToken); //Méthode delete existant de base dans un repo. Ici un id en paramètre est suffisant pour que JPA supprime le bon de la BDD, l'objet Token aurait fonctionné aussi
        }
    }

    @Override
    public void saveNewToken(Token token) {
        if(token != null){
            tokenRepository.save(token);
        }
    }

}
