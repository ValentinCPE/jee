package com.cpe.jee.service.impl;

import com.cpe.jee.exception.*;
import com.cpe.jee.model.Card;
import com.cpe.jee.model.User;
import com.cpe.jee.model.dto.*;
import com.cpe.jee.model.view.CardView;
import com.cpe.jee.model.view.UserView;
import com.cpe.jee.repository.CardRepository;
import com.cpe.jee.service.CardService;
import com.cpe.jee.service.TokenService;
import com.cpe.jee.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.*;

@Service
public class CardServiceImpl implements CardService {


    @Autowired
    private CardRepository cardRepository;

    //Référence à un service dédié token et non pas un repo autre que User
    @Autowired
    private TokenService tokenService;

    @Autowired
    private UserService userService;

    @PostConstruct
    private void addCardAndLinkUser() throws AcceptationCriteriaRefusedException, ParameterException, AccountAlreadyExistsException {
     //   INSERT INTO `card` VALUES ('2222222222222','qsdqsd','Marvel','50','1','1','1','http://localhost:8080/WebServicecard/create','5c7432d6-0d65-42b7-a92b-3af1fac199f7'),('myHERO',NULL,'DC Comics','3000','500','1','0','https://combien.io/wp-content/uploads/2017/02/super-hero-marvel-678x381.jpg','newID'),('leonardo','the best one','Ninja turtle','50','10','5','20','https://cdn.movieweb.com/img.news.tops/NEvRS174lkloyx_1_b/Teenage-Mutant-Ninja-Turtles-Leonardo-Character-Poster.jpg','sdfsdfsdf-sdfsdfdsf'),('qsdqsdqsd','qsdqsdqsd','DC Comics','50','50','14','10','qsqsdqsdqsdqsdqdqsdqsdqsdqsd','swag');
        Card card1 = Card.builder()
                .name("2222222222222")
                .description("qsdqsd")
                .family("Marvel")
                .hp(58)
                .attack(1)
                .defence(1)
                .energy(1)
                .imgUrl("http://localhost:8080/WebServicecard/create")
                .enVente(false)
                .build();

        Card card2 = Card.builder()
                .name("myHERO")
                .description(null)
                .family("DC Comics")
                .hp(3000)
                .attack(500)
                .defence(1)
                .energy(0)
                .imgUrl("https://combien.io/wp-content/uploads/2017/02/super-hero-marvel-678x381.jpg")
                .enVente(false)
                .build();

        Card card3 = Card.builder()
                .name("leonardo")
                .description("the best one")
                .family("Ninja turtle")
                .hp(58)
                .attack(10)
                .defence(5)
                .energy(20)
                .imgUrl("https://cdn.movieweb.com/img.news.tops/NEvRS174lkloyx_1_b/Teenage-Mutant-Ninja-Turtles-Leonardo-Character-Poster.jpg")
                .enVente(false)
                .build();

        cardRepository.save(Arrays.asList(card1,card2,card3));

        UserDto userDto = UserDto.builder()
                .mail("test@test.fr")
                .name("test")
                .password("test123")
                .build();

        userService.create(userDto);

        UserDto userDto1 = UserDto.builder()
                .mail("test2@test.fr")
                .name("test")
                .password("test123")
                .build();

        userService.create(userDto1);

        User user = userService.getUserFromMail(userDto.getMail());
        user.getCards().add(card1);
        userService.saveUser(user);

        User user1 = userService.getUserFromMail(userDto1.getMail());
        user1.setSolde(100.0);
        userService.saveUser(user1);

    }

    @Override
    public Iterable<CardView> listCards(String token) throws NoCardException, NotConnectedException {
        if(!userService.isConnected(token)){
            throw new NotConnectedException(HttpStatus.UNAUTHORIZED);
        }

        Iterable<Card>  listCard = cardRepository.findAll();

        if(listCard == null){
            throw new NoCardException(HttpStatus.UNAUTHORIZED);
        }

        return CardView.getIterableCardViewFromIterableCard(listCard);

    }

    @Override
    public CardView create(String token, CardDto card) throws NotConnectedException, CardAlreadyExistException, ParameterException {
        if(!userService.isConnected(token)){
            throw new NotConnectedException(HttpStatus.UNAUTHORIZED);
        }

        if(card == null){
            throw new ParameterException(HttpStatus.BAD_REQUEST, "Form JSON");
        }

        Card testCard = cardRepository.findCardByName(card.getName());

        if(testCard != null){
            throw new CardAlreadyExistException(HttpStatus.UNAUTHORIZED);
        }

        Card newCard = Card.builder()
                .name(card.getName())
                .description(card.getDescription())
                .family(card.getFamily())
                .hp(card.getHp())
                .energy(card.getEnergy())
                .defence(card.getDefence())
                .attack(card.getAttack())
                .imgUrl(card.getImgUrl())
                .enVente(false)
                .build();

        cardRepository.save(newCard);

        return CardView.getCardViewFromCard(newCard);

    }

    @Override
    public CardView getCardByName(String token, String name) throws NotConnectedException, ParameterException, CardDoesntExistException {
        if(!userService.isConnected(token)){
            throw new NotConnectedException(HttpStatus.UNAUTHORIZED);
        }

        if(name == null){
            throw new ParameterException(HttpStatus.BAD_REQUEST, "Form JSON");
        }

        Card foundCard = cardRepository.findCardByName(name);

        if(foundCard == null){
            throw new CardDoesntExistException(HttpStatus.UNAUTHORIZED);
        }

        return CardView.getCardViewFromCard(foundCard);
    }

    @Override
    public List<CardView> vendreCards(String token, VenteCardDto venteCardDto) throws NotConnectedException {
        User user = userService.getUser(token);

        if(user == null){
            throw new NotConnectedException(HttpStatus.UNAUTHORIZED);
        }

        List<CardView> cardsEnVente = new ArrayList<>();

        for(CardPriceDto cardPriceDto : venteCardDto.getCards()){
            Card card = cardRepository.findCardByName(cardPriceDto.getNameCard());

            //Si carte existe et appartient au User
            if(card != null && user.getCards().contains(card)){
                card.setEnVente(true);
                card.setPrice(cardPriceDto.getPrice());
                cardRepository.save(card);
                cardsEnVente.add(CardView.getCardViewFromCard(card));
            }
        }

        return cardsEnVente;

    }

    @Override
    public UserView achatCards(String token, AchatCardDto achatCardDto) throws NotConnectedException {
        User user = userService.getUser(token);

        if(user == null){
            throw new NotConnectedException(HttpStatus.UNAUTHORIZED);
        }

        for(String cardAchetee : achatCardDto.getCardsAchetees()){
            Card card = cardRepository.findCardByName(cardAchetee);

            if(card != null && card.isEnVente() && user.getSolde() > card.getPrice()){
                //Mise à jour ancien user
                User oldOwner = userService.getUserWithCard(card);

                if(oldOwner != user){
                    oldOwner.setSolde(oldOwner.getSolde() + card.getPrice());
                    oldOwner.getCards().remove(card);
                    userService.saveUser(oldOwner);

                    //Mise à jour nouveau user
                    user.setSolde(user.getSolde() - card.getPrice());
                    user.getCards().add(card);
                    userService.saveUser(user);

                    //Mise à jour carte
                    card.setEnVente(false);
                    cardRepository.save(card);
                }
            }
        }

        return UserView.getUserViewFromUser(user);
    }

    @Override
    public Set<Card> getFiveRandomCard() {
        Random rand = new Random();
        Set<Card> listCards = new HashSet<>();
        ArrayList<Card> cards = new ArrayList<>();
        cardRepository.findAll().forEach(cards::add);
        for(int i = 0; i < 5; i++){
            int randNumber = rand.nextInt(cards.size());
            listCards.add(cards.get(randNumber));
        }
        return listCards;
    }
}
