package com.cpe.jee.service;

import com.cpe.jee.exception.*;
import com.cpe.jee.model.Card;
import com.cpe.jee.model.User;
import com.cpe.jee.model.dto.UserDto;
import com.cpe.jee.model.view.UserView;
import org.springframework.stereotype.Service;

@Service
public interface UserService {

    /**
     * Return UserView with name in parameter
     * @param name : String
     * @return UserView object
     */
    UserView getUserFromName(String name) throws ParameterException;

    /**
     * Check login user with JSON Object passed in parameter
     * @param userFormulaire : Object representing the form
     * @return User logged in with token generated
     */
    UserView login(UserDto userFormulaire) throws ParameterException, MailNotFoundException, PasswordIncorrectException;

    /**
     * Create new user with infos in UserDto object
     * @param userFormulaire : Object representing the form
     * @return New User created not logged in, with token null
     */
    UserView create(UserDto userFormulaire) throws ParameterException, AccountAlreadyExistsException, AcceptationCriteriaRefusedException;

    /**
     * Return Unauthorized if token null or not valid in DB
     * @param token : token passed in header in the HTTP request
     * @return UserView linked to token
     */
    UserView getUserWithToken(String token) throws NotConnectedException;

    /**
     * @param token : token passed in header in the HTTP request
     * @return UserView linked to token
     */
    UserView logout(String token) throws NotConnectedException;

    boolean isConnected(String token);

    User getUser(String token);
    
    User getUserWithCard(Card card);

    void saveUser(User user);

    User getUserFromMail(String mail);
}
