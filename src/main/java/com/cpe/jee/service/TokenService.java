package com.cpe.jee.service;

import com.cpe.jee.model.Token;
import org.springframework.stereotype.Service;

@Service
public interface TokenService {

   /**
    * Remove token according idToken
    * @param idToken : id of token
    */
   public void removeToken(Integer idToken);

   /**
    * create new Token in DB
    * @param token : object JAVA to add in DB
    */
   public void saveNewToken(Token token);

}
