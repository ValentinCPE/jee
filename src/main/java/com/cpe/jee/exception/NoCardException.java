package com.cpe.jee.exception;

import org.springframework.http.HttpStatus;

public class NoCardException extends GeneralHandlerException {
    public NoCardException(HttpStatus httpStatus){
        super(httpStatus);
    }

    @Override
    public String getMessage() {
        return " Il n'existe aucune carte";
    }
}
