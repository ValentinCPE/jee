package com.cpe.jee.exception;

import org.springframework.http.HttpStatus;

public class ParameterException extends GeneralHandlerException {

    private String nameParameter;

    public ParameterException(HttpStatus httpStatus, String nameParameter){
        super(httpStatus);
        this.nameParameter = nameParameter;
    }

    @Override
    public String getMessage() {
        return "Le paramètre " + this.nameParameter + " n'est pas correct";
    }
}
