package com.cpe.jee.exception;

import org.springframework.http.HttpStatus;

public class PasswordIncorrectException extends GeneralHandlerException {

    public PasswordIncorrectException(HttpStatus httpStatus){
        super(httpStatus);
    }

    @Override
    public String getMessage() {
        return "Le mot de passe entré n'est pas correct";
    }

}
