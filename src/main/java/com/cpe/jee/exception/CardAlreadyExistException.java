package com.cpe.jee.exception;

import org.springframework.http.HttpStatus;

public class CardAlreadyExistException extends GeneralHandlerException {

    public CardAlreadyExistException(HttpStatus httpStatus){
        super(httpStatus);
    }

    @Override
    public String getMessage() {
        return "La carte existe deja";
    }
}
