package com.cpe.jee.exception;

import org.springframework.http.HttpStatus;

public class MailNotFoundException extends GeneralHandlerException {

    private String mail;

    public MailNotFoundException(HttpStatus httpStatus, String mail){
        super(httpStatus);
        this.mail = mail;
    }

    @Override
    public String getMessage() {
        return "L'adresse mail " + this.mail + " n'a pas de compte chez nous";
    }
}
