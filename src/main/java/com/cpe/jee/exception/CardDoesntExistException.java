package com.cpe.jee.exception;

import org.springframework.http.HttpStatus;

public class CardDoesntExistException extends GeneralHandlerException {

    public CardDoesntExistException(HttpStatus httpStatus){
        super(httpStatus);
    }

    @Override
    public String getMessage() {
        return "La carte n'existe pas";
    }
}
