package com.cpe.jee.exception;

import org.springframework.http.HttpStatus;

public class AcceptationCriteriaRefusedException extends GeneralHandlerException {

    public AcceptationCriteriaRefusedException(HttpStatus httpStatus){
        super(httpStatus);
    }

    @Override
    public String getMessage() {
        return "Les critères d'acceptation d'une adresse et d'un mot de passe ne sont pas respectés par votre saisie";
    }
}
