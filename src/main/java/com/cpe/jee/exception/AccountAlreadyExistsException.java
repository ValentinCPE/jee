package com.cpe.jee.exception;

import org.springframework.http.HttpStatus;

public class AccountAlreadyExistsException extends GeneralHandlerException {

    private String mail;

    public AccountAlreadyExistsException(HttpStatus httpStatus, String mail){
        super(httpStatus);
        this.mail = mail;
    }

    @Override
    public String getMessage() {
        return "Un compte existe déjà avec l'adresse " + this.mail;
    }
}
