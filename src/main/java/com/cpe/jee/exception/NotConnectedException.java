package com.cpe.jee.exception;

import org.springframework.http.HttpStatus;

public class NotConnectedException extends GeneralHandlerException {

    public NotConnectedException(HttpStatus httpStatus){
        super(httpStatus);
    }

    @Override
    public String getMessage() {
        return "Token de connexion inexistant ou expiré";
    }
}
