package com.cpe.jee.exception;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.http.HttpStatus;

@Data
@NoArgsConstructor
public abstract class GeneralHandlerException extends Exception {

    private HttpStatus statusHttp = HttpStatus.INTERNAL_SERVER_ERROR; //Par défault, une erreur 500 est renvoyée si cette variable non modifiée

    GeneralHandlerException(HttpStatus httpStatus){
        super();
        this.statusHttp = httpStatus;
    }

    @Override
    public abstract String getMessage();

}
