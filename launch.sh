#!/bin/bash

./mvninstall.sh

export JAVA_HOME="$PWD/jdk"

mvn clean
mvn install -Dmaven.test.skip=true

docker-compose up --force-recreate --build